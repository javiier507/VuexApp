import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import router from './routes'
import store from './store'

Vue.config.productionTip = false

//  GUARDS
router.beforeEach((to, from, next) => {
    if(to.matched.some(x => x.meta.requiresAuth)) {
        if(store.getters.isLogin)
            next();
        else
            next({name: 'login'});
    }
    else {
        if(store.getters.isLogin == true && (to.name == 'login' || to.name == 'register'))
            next({name: 'Home'});
        else
            next();
    }
});

//  INSTANCE
new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});
