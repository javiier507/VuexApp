import Vue from 'vue';
import Vuex from 'vuex';

import tasks from '@/store/modules/Tasks';
import auth from '@/store/modules/Auth';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        tasks,
        auth
    }
})
