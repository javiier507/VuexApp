import { http } from '@/config'

export default {

    state : {
        tasks: [],
        task: {
            title: '',
            description: ''
        },
        developer: 'carlos penalba'
    },

    getters: {
        tasks(state) {
            return state.tasks;
        },
        task(state) {
            return state.task;
        },
        developer(state) {
            return state.developer;
        }
    },

    mutations: {
        setTasks(state, payload) {
            state.tasks = payload;
        },

        setTaskById(state, payload) {
            state.task = payload;
        },

        clearTask(state) {
            state.task = {
                title: '',
                description: ''
            }
        }
    },

    actions: {

        getTasks({commit}) {
            return http.get('tasks').then((response) => {
                commit('setTasks', response.data);
            })
            .catch((error) => {
                console.log(error);
            });
        },

        getTaskById({commit}, id) {
            return http.get(`tasks/${id}`).then((response) => {
                commit('setTaskById', response.data);
            })
            .catch((error) => {
                console.log(error);
            });
        },

        createTaks({commit}, task) {
            return new Promise((resolve, reject) => {
                return http.post('tasks', task).then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
            });
        },

        updateTaks({commit}, task) {
            return new Promise((resolve, reject) => {
                http.put(`tasks/${task._id}`, task).then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
            });
        },

        deleteTaks({commit}, id) {
            return http.delete(`tasks/${id}`);
        }
    }
}
