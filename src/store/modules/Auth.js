import { http } from '@/config';

export default {

    state: {
        isLogin: !!localStorage.getItem('_token'),
        user: localStorage.getItem('_user')
    },

    getters: {
        isLogin(state) {
            return state.isLogin;
        },
        user(state) {
            return state.user;
        }
    },

    mutations: {
        setLogin(state, flag) {
            state.isLogin = flag;
        },
        setUser(state) {
            var jwtDecode = require('jwt-decode');
            let decode = jwtDecode(localStorage.getItem('_token'));
            localStorage.setItem('_user', decode.data);
            state.user = decode.data;
        }
    },

    actions: {
        login({commit}, user) {
            return http.post('auth/login', user).then((response) => {
                localStorage.setItem('_token', response.data.token);
                commit('setLogin', true);
                commit('setUser');
            }).catch((error) => {
                console.log(error);
            });
        },
        logout({commit}) {
            /*localStorage.removeItem('_token');
            localStorage.removeItem('_user');*/
            localStorage.clear();
            commit('setLogin', false);
        },
        getProfile({commit}) {
            return new Promise((resolve, reject) => {
                http.get('auth/profile').then((response) => {
                    resolve(response);
                }).catch((error)=> {
                    reject(error);
                });
            });
        }
    }
}
