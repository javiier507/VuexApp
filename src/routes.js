import Vue from 'vue';
import VueRouter from 'vue-router'

import Home from '@/components/Home';

//  TASKS
import TaskCreate from '@/components/Tasks/TaskCreate';
import TasksList from '@/components/Tasks/TasksList';
import TaskShow from '@/components/Tasks/TaskShow';
import TaskEdit from '@/components/Tasks/TaskEdit';

//  AUTH
import Index from '@/components/Auth/Index';
import Login from '@/components/Auth/Login';
import Register from '@/components/Auth/Register';
import Profile from '@/components/Auth/Profile';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/task-new',
        name: 'TaskCreate',
        component: TaskCreate
    },
    {
        path: '/tasks',
        name: 'TasksList',
        component: TasksList
    },
    {
        path: '/tasks/:id',
        name: 'TaskShow',
        component: TaskShow
    },
    {
        path: '/task-edit/:id',
        name: 'TaskEdit',
        component: TaskEdit
    },
    {
        path: '/auth',
        component: Index,
        children: [
            {
                path: '',
                name: 'login',
                component: Login
            },
            {
                path: 'register',
                name: 'register',
                component: Register
            },
            {
                path: 'profile',
                name: 'profile',
                component: Profile,
                meta: { requiresAuth: true }
            }
        ]
    }
]

export default new VueRouter({
    routes
})
